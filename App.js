import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {Provider} from './src/context/ContextProvider';
import RouterScreen from './src/screens/RouterScreen';
import DashboardScreen from './src/screens/DashboardScreen/';
const rootNavigator = createStackNavigator(
  {
    Router: RouterScreen,
    Dashboard: DashboardScreen,
  },
  {
    initialRouteName: 'Dashboard',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  },
);

const App = createAppContainer(rootNavigator);

export default () => (
  <Provider>
    <App />
  </Provider>
);

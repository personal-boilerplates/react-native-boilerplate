import constants from '../constants';

const {PAGE_TOGGLE_LOADER, PAGE_SET_ERROR} = constants;

export default {
  toggleLoader: dispatch => async payload => {
    dispatch({type: PAGE_TOGGLE_LOADER, payload});
  },

  setError: dispatch => async payload => {
    dispatch({type: PAGE_SET_ERROR, payload});
  },
};

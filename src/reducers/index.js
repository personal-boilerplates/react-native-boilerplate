import cartReducer from './cartReducer';
import conciergeCartReducer from './conciergeCartReducer';
import userReducer from './userReducer';
import screenReducer from './screenReducer';
import serviceSelectReducer from './serviceSelectReducer';
export default {
  cartReducer,
  conciergeCartReducer,
  userReducer,
  screenReducer,
  serviceSelectReducer,
};

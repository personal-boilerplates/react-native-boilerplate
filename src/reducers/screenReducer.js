import constants from '../constants';

const { PAGE_TOGGLE_LOADER, PAGE_SET_ERROR } = constants;

export default (state, action) => {
  let { type, payload } = action;
  switch (type) {
    case PAGE_TOGGLE_LOADER: {
      return {
        ...state,
        loadingOpen: !!payload,
      };
    }
    case PAGE_SET_ERROR: {
      return {
        ...state,
        errors: payload,
      };
    }
    default:
      return state;
  }
};

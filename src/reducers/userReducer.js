import constants from '../constants';

const {SET_USER} = constants;

export default (state, action) => {
  switch (action.type) {
    case SET_USER: {
      return action.payload;
    }
    default:
      return state;
  }
};

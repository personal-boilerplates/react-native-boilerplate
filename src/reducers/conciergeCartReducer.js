import constants from '../constants';

const {
  ADD_CONCIERGE_MERCHANT,
  DELETE_CONCIERGE_ITEM,
  SET_CONCIERGE_MERCHANTS,
  CLEAR_CONCIERGE_MERCHANTS,
  SET_TEMP_CONCIERGE_MERCHANTS,
  CLEAR_TEMP_CONCIERGE_MERCHANTS,
} = constants;

export default (state, action) => {
  switch (action.type) {
    case ADD_CONCIERGE_MERCHANT: {
      return {...state, merchants: [...state.merchants, ...action.payload]};
    }
    case DELETE_CONCIERGE_ITEM: {
      const {merchantName, index} = action.payload;
      const newMerchantList = state.merchants.map(merchant => {
        if (merchant.name === merchantName) {
          let newProducts = merchant.products.filter(
            product => product.index !== index,
          );
          return {...merchant, products: newProducts};
        } else return merchant;
      });
      return {
        ...state,
        merchants: newMerchantList.filter(
          merchant => merchant.products.length !== 0,
        ),
      };
    }
    case SET_CONCIERGE_MERCHANTS: {
      return {
        ...state,
        merchants: action.payload,
      };
    }
    case CLEAR_CONCIERGE_MERCHANTS: {
      return {
        ...state,
        merchants: [],
      };
    }
    case SET_TEMP_CONCIERGE_MERCHANTS: {
      return {
        ...state,
        tempMerchants: action.payload,
      };
    }
    case CLEAR_TEMP_CONCIERGE_MERCHANTS: {
      return {
        ...state,
        tempMerchants: [],
      };
    }
    default:
      return state;
  }
};

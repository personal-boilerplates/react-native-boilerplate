import constants from '../constants';

const { SET_SERVICE_SELECTION, TOGGLE_SERVICE_SELECTION } = constants;

export default (state, action) => {
  let { type, payload } = action;
  switch (type) {
    case SET_SERVICE_SELECTION: {
      return {
        ...state,
        value: payload
      };
    }

    case TOGGLE_SERVICE_SELECTION: {
      return {
        ...state,
        isShow: payload
      };
    }

    default:
      return state;
  }
};

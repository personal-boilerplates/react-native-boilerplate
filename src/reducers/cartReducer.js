import constants from '../constants';

const {
  ADD_CART_ITEM,
  REMOVE_CART_ITEM,
  ADD_TEMP_CART_ITEM,
  REMOVE_TEMP_CART_ITEM,
  CLEAR_CART,
} = constants;

export default (state, action) => {
  switch (action.type) {
    case ADD_CART_ITEM: {
      return {...state, products: [...state.products, action.payload]};
    }
    case REMOVE_CART_ITEM: {
      return {
        ...state,
        products: [
          ...state.products.filter((item, index) => index !== action.payload),
        ],
      };
    }
    case ADD_TEMP_CART_ITEM: {
      return {...state, tempProduct: action.payload};
    }
    case REMOVE_TEMP_CART_ITEM: {
      return {
        ...state,
        tempProduct: null,
      };
    }
    case CLEAR_CART: {
      return {
        ...state,
        products: [],
      };
    }
    default:
      return state;
  }
};

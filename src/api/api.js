import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import constants from '../constants';

let {USER_CREDENTIALS} = constants;
const {getItem} = AsyncStorage;

let axiosInstance = axios.create({
  // baseURL: 'http://192.168.100.104:4000',
  // baseURL: `https://dev.grooverdelivery.ph/core`,
  // baseURL: 'http://localhost:4000',
  baseURL: `https://grooverdelivery.ph/core`,
});

axiosInstance.interceptors.request.use(async function(config) {
  const loginData = JSON.parse(await getItem(USER_CREDENTIALS));

  if (loginData && loginData.token) {
    config.headers.common['authorization'] = `Bearer ${loginData.token}`;
  } else {
    delete config.headers.common['authorization'];
  }
  return config;
});

axiosInstance.interceptors.response.use(
  function(response) {
    if (response.status === 401) {
      // do something
    }
    return response;
  },
  function(error) {
    let {response} = error;
    // console.log({api_error: error});
    if (response) {
      if (response.config.noError) {
        return Promise.reject(error); //use to return no error popup after api call
      }
      if ([401].includes(response.status)) {
        // usually unaothorized
        //do something
        let errorMessage = 'Invalid access token (may have expired)';
        if (response && response.data) {
          errorMessage = response.data.message;
        }
        //send errorMessage to redux
      } else if ([400].includes(response.status)) {
        let errorMessage = `Sorry, something went wrong. We're working on it and we'll get it fixed as soon as we can.`;
        if (response.data.message) {
          errorMessage = response.data.message;
        }
        //send errorMessage to redux
      }
    }
    return Promise.reject(error);
  },
);

export default axiosInstance;

import React, {useReducer} from 'react';
import Reducers from '../reducers';
import Actions from '../actions';
import {initialScreenState} from './initialStates';

const bindActions = (actions, dispatch) => {
  let boundActions = {};

  for (let key in actions) {
    boundActions[key] = actions[key](dispatch);
  }

  return boundActions;
};

export default () => {
  const Context = React.createContext();
  const Provider = ({children}) => {
    const [screenState, screenDispatch] = useReducer(
      Reducers.screenReducer,
      initialScreenState,
    );

    const boundScreenActions = bindActions(
      Actions.screenActions,
      screenDispatch,
    );

    return (
      <Context.Provider
        value={{
          screenState,
          ...boundScreenActions,
        }}>
        {children}
      </Context.Provider>
    );
  };

  return {Context, Provider};
};

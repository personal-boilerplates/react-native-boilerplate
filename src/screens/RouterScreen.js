import React, {useEffect, useContext} from 'react';
import {NavigationActions, StackActions} from 'react-navigation';

const RouterScreen = ({navigation}) => {
  const redirect = async () => {
    let navigateTo = 'Dashboard';
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: navigateTo})],
    });
    let action = resetAction;
    navigation.dispatch(action);
  };
  useEffect(() => {
    redirect();
  }, []);

  return <></>;
};
RouterScreen.navigationOptions = navigate => {
  return {
    header: null,
  };
};

export default RouterScreen;
